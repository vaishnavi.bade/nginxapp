# Stage 1: Build
FROM node:18-alpine

WORKDIR /app


RUN npm install
COPY package.json ./

RUN npm run build

# Stage 2: Runtime
FROM nginx:latest

COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 80

# Nginx is the default command, no need to specify it

